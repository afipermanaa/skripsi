//
//  MainController.swift
//  
//
//  Created by Jamsey Bond🤪 on 06/05/20.
//


//yg ada no nya which means is the step by step of using firebase

import UIKit
import Foundation
import Firebase
import FirebaseDatabase

class MainController: UITableViewController, AddPatientController {
    
    private var patientLists = [PatientList]() // empty array buat isi list yg isinya nama pasien
    var Segue : String = "PatientName"
    var Segue2 : String = "PatientNotes"
    let user : User = Auth.auth().currentUser! // ini buat akses usernya, buat akses user yg lg login skrng
    
    private var rootRef : DatabaseReference!// 1. buat nyambung ke root db

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.rootRef = Database.database().reference() // 2. ini nyambungin ke rootnya di firebase
        populateList()
        
//        self.navigationController?.navigationBar.prefersLargeTitles = true
//        self.navigationController?.navigationItem.largeTitleDisplayMode

    }
    
    private func populateList() {// 5. func buat fetch data dari db ke hp
        
        // value nya itu buat kl data yg di db nya ada yg berubah, nanti ke trigger value apa yg berubah dr si dbnya, jd tau apa yg berubah dan berubahnya jadi apa
        // snapshot itu berisi root nya, yg dmn root nya berisi dr child yg isinya nama2 pasien
        
        self.rootRef.child(self.user.emailWithoutSpecialChar).observe(.value) { (snapshot) in //6. ini buat ngeliat value dan data dr db firebase
            
            self.patientLists.removeAll()
            
            let pasienListDict = snapshot.value as? [String:Any]  ?? [:] //7. ini berarti return buat kl dict nya kosong, ini buat akses ke valuenya yg isinya itu dict[String:Any]
            
            for (key,_) in pasienListDict {
                
                if let pasienlistdict = pasienListDict[key] as? [String:Any]{
                // 8. ini buat akses property dr dbnya di firebase, yg isinya itu key dan value. key -> Name(var nama si pasien),  value-> "Afi Permana" (isi dr variable si pasien itu)
                    
                    if let pasienlist = PatientList(pasienlistdict) {
                        self.patientLists.append(pasienlist)
                        // ini buat ngemasukin ke dalem dictionarynya, ini buat store datanya dan ngambil datanya dari firebase db
                    }
                    
                }
            }
            
            DispatchQueue.main.async {
                self.tableView.reloadData()
            }
            
        }
        
    }
     
    func addPatientNameSave(controller: UIViewController, title: String) { // data yg di simpen di protocol nya di keluarin disini, langsung keluar pas
        
        let patientList = PatientList(name: title)
        self.patientLists.append(patientList)
        
        let userRef = self.rootRef.child(self.user.emailWithoutSpecialChar)// 9. ini buat masuk ke userRef di firebasenya
        let patientListRef = userRef.child(patientList.name)// 3. reference buat ke node nya
        
        patientListRef.setValue(patientList.toDictionary()) // 4. ini buat add namanya ke child di database root firebase
        // karena yg di input nama pasien jd yg gw masukin namanya di dictionarynya
        
        
        //--> jadi kalo udh nyambung ke userRefnya, semua pasienlist yg ada di dictionary akan ke save di tiap2 userRef yg login, jadi tiap user udh bisa beda2 listnya.
        controller.dismiss(animated: true, completion: nil)

                
        DispatchQueue.main.async {
            self.tableView.reloadData()
        }
        
        
    }
    
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            let pasienList = self.patientLists[indexPath.row]
            let pasienListRef = self.rootRef.child(pasienList.name)
            pasienListRef.removeValue()
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == Segue {
            let nc = segue.destination as! UINavigationController
            let addPatientName = nc.viewControllers.first as! AddListController
            addPatientName.delegate = self
            
            }
            
            else if segue.identifier == Segue2 {
            
            guard let indexPath = self.tableView.indexPathForSelectedRow else {return}
             
            let nc = segue.destination as! PasienProfileController
            print("ini datanya 😘 \(nc.pasien = self.patientLists[indexPath.row])") // .pasien nya itu variable nyambung dari pasienprofilecontroller O
            
    
        }
    
    }

//    override func numberOfSections(in tableView: UITableView) -> Int {
//
//        return 0
//    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return self.patientLists.count
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath)
        let patientList = self.patientLists[indexPath.row]
        
        cell.textLabel?.text = patientList.name


        return cell
    }
    

    
}


    
    
    

