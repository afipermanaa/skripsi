
//  NotesController.swift
//  GroceryList_FB

//  Created by Jamsey Bond🤪 on 02/05/20.
//  Copyright © 2020 Jamsey Bond🤪. All rights reserved.

import UIKit
import Foundation
import Firebase

class NotesController: UITableViewController, AddNotesDelegate {
    
    
    var pasien : PatientList!// buat manggil si class PatientListnya
    private var rootRef : DatabaseReference! //1. var buat ambil root db firebase
    
    
    
    var Segue1 : String = "AddNotes"
    var Segue2 : String = "PasienNotes"

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = pasien.name
        
        self.rootRef = Database.database().reference()
        
    }

    func AddNotes(controller: UIViewController, notes: PatientNote) {
        
        guard let user = Auth.auth().currentUser,
            let email = user.email else {return} // ini buat get user nya yg login
        
        let userRef = self.rootRef.child(email.removeSpecialChar()) // ini buat masuk ke userRef di firebase sesuai dgn yg login siapa
        
        let pasienListRef = userRef.child(self.pasien.name)//2. ini buat nyari child dr si parent nama pasiennya, jd nanti nambahnya ke dlm child si parentnya gituuu
        
        
        
        if let selectedIndexPath = tableView.indexPathForSelectedRow {
            
            DispatchQueue.main.async { // ini di select pas mau di edit
                self.pasien.patientNote[selectedIndexPath.row] = notes
                pasienListRef.setValue(self.pasien.toDictionary())
                self.tableView.reloadRows(at: [selectedIndexPath], with: .none)
            }
        }else {// kalo ini blm di edit, jadi kl di pencet tombol add new notes gt
            self.pasien.patientNote.append(notes)
            
            pasienListRef.setValue(self.pasien.toDictionary())
            
            DispatchQueue.main.async {
                self.tableView.reloadData()
            }
            
            controller.dismiss(animated: true, completion: nil)
        }
        
    }
    
    //Mark : Override Func
    
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) { //delete func

        if editingStyle == .delete {
            
//            let pasienNote = self.pasien.patientNote[indexPath.row]
//                           let pasienNoteRef = self.rootRef.child(pasienNote.note)
//                           pasienNoteRef.removeValue()

                           let pasienNotes = self.pasien.patientNote.remove(at: indexPath.row)//ini buat delete dari indexpath nya yg dipilih dari tableview
                
                           self.tableView.reloadData()
        }else{
            print(Error.self)
        }
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return self.pasien.patientNote.count
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == Segue1 {
            guard let nc = segue.destination as? UINavigationController else {return}
            guard let addPatientNotes = nc.viewControllers.first as? AddNotesController else {return}
            addPatientNotes.delegate = self
        }
        else if segue.identifier == Segue2 {
             
            guard let indexPath = self.tableView.indexPathForSelectedRow else {return}
            guard let nc = segue.destination as? UINavigationController else {return}
            let data = self.pasien.patientNote[indexPath.row]
            guard let notesPasien = nc.viewControllers.first as? NotePatientController else {return}
            
            notesPasien.pasienNotes = data.note
            notesPasien.delegate = self

//            let data = self.pasien.patientNote[indexPath.row]

//            guard let vc = storyboard?.instantiateViewController(identifier: "PasienNotess") as? NotePatientController else {return}
//
//            vc.pasienNotes = data.note
//
//            navigationController?.present(vc, animated: true, completion: nil)
            
            
        }
    }
    

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath)
        let data = self.pasien.patientNote[indexPath.row]

        cell.textLabel?.text = data.note

        return cell
    }
  

}

