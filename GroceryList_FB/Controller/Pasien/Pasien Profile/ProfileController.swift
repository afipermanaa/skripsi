//
//  ProfileController.swift
//  GroceryList_FB
//
//  Created by Jamsey Bond🤪 on 14/06/20.
//  Copyright © 2020 Jamsey Bond🤪. All rights reserved.
//

import UIKit
import Firebase


protocol AddPatientControllerr {
    
    func addPatientData(controller: UIViewController, title: String)
    // ini buat nyimpen data yang udh di add di textfieldnya, data ke save di memorinya
    
}



class ProfileController: UIViewController,UIPickerViewDelegate, UIPickerViewDataSource {
    

    // MARK: textfield
    @IBOutlet weak var namaTxt: UITextField!
    @IBOutlet weak var TglLahirTxt: UITextField!
    @IBOutlet weak var noTelfonTxt: UITextField!
    @IBOutlet weak var beratBadanTxt: UITextField!
    @IBOutlet weak var TinggiBadanTxt: UITextField!
    @IBOutlet weak var GolDarahTxt: UITextField!
    
    
    // MARK : Var and Let
    
    let datePicker = UIDatePicker()
    let pickerView = UIPickerView()
    
    // MARK : Data Gol Darah
    
    var golDarah = ["A", "A+", "A-", "B", "B+", "B-", "AB", "AB+", "AB-", "O", "O+", "O-", "N/A"]
    var selectGolDarah : String?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        
        
        createBtn()//ini buat tgl lahir
        golDarahBtn() // ini buat gol darah
    }
    
    
    
    // MARK: Button
    @IBAction func changePic(_ sender: Any) {
        
    }
    @IBAction func saveBtn(_ sender: Any) {
        
        
        
    }
    
    
    //MARK : Function buat tgl lahir
    
    func createBtn() {
        
        //textfield middle
        TglLahirTxt.textAlignment = .center
        
        //toolbar
        let toolbar = UIToolbar()
        toolbar.sizeToFit()
        
        //bar button
        let doneBtn = UIBarButtonItem(barButtonSystemItem: .done, target: nil, action: #selector(donePressed))
        toolbar.setItems([doneBtn], animated: true)
        
        //Assign toolbar ke dlm textfield
        TglLahirTxt.inputAccessoryView = toolbar
        
        //assign datepicker ke dlm textfield
        TglLahirTxt.inputView = datePicker
        
        //datepicker tgl
        datePicker.datePickerMode = .date
    }
    
    @objc func donePressed() {
        //formatter
        let formatter = DateFormatter()
        formatter.dateStyle = .medium
        formatter.timeStyle = .none
        
        
        TglLahirTxt.text = formatter.string(from: datePicker.date)
        self.view.endEditing(true)
    }
    
    
    
    // MARK : UIPickerView
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return golDarah.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return golDarah[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        selectGolDarah = golDarah[row]
        GolDarahTxt.text = selectGolDarah
    }
    
    func golDarahBtn() {
        
        GolDarahTxt.textAlignment = .center
        
        let toolBar = UIToolbar()
        toolBar.sizeToFit()
        
        let doneBTN = UIBarButtonItem(barButtonSystemItem: .done, target: nil, action: #selector(done))
        toolBar.setItems([doneBTN], animated: true)
        
        GolDarahTxt.inputAccessoryView = toolBar
        
        GolDarahTxt.isUserInteractionEnabled = true
        
    }
    
    @objc func done() {
        self.view.endEditing(true)
    }
    
    

}
