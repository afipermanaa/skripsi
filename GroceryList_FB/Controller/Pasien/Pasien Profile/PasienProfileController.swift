//
//  PasienProfileController.swift
//  GroceryList_FB
//
//  Created by Jamsey Bond🤪 on 08/06/20.
//  Copyright © 2020 Jamsey Bond🤪. All rights reserved.
//

import UIKit
import Firebase

class PasienProfileController: UIViewController {

    @IBOutlet weak var TinggiLbl: UILabel!
    @IBOutlet weak var beratLbl: UILabel!
    @IBOutlet weak var GolDarahLbl: UILabel!
    @IBOutlet weak var NamaLbl: UILabel!
    
    @IBOutlet weak var ImagePic: UIImageView!
    
    var pasien : PatientList!
    private var rootRef : DatabaseReference!
    
    @IBOutlet weak var ContainerView: UIView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.ImagePic.makeRounded()
        self.NamaLbl.text = pasien.name

        // Do any additional setup after loading the view.
    }
    
    @IBAction func EditDataBtn(_ sender: Any) {
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
           
           if let statiView = segue.destination as? PasienTableController {
               
            var destVC = segue.destination as! PasienTableController
               statiView.delegete = self
            
//            destVC.delegete = pasien as! PasienTableControllerDelegate
                
                
           }
       }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension PasienProfileController : PasienTableControllerDelegate {
    
    func segway() {
        print("Test 😈")
    }
    
    
}
