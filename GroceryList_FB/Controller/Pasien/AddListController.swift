//
//  AddListController.swift
//  GroceryList_FB
//
//  Created by Jamsey Bond🤪 on 02/05/20.
//  Copyright © 2020 Jamsey Bond🤪. All rights reserved.


import UIKit
import Firebase


protocol AddPatientController {
    
    func addPatientNameSave(controller: UIViewController, title: String)
    // ini buat nyimpen data yang udh di add di textfieldnya, data ke save di memorinya
    
}

class AddListController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        
    }
    
    var delegate : AddPatientController!
    

    @IBOutlet weak var AddList: UITextField!
    
    @IBAction func AddBtn(_ sender: Any) {
        
        if let title = self.AddList.text{
            self.delegate.addPatientNameSave(controller: self, title: title) // ini func buat nyimpennya
        }
    }

}
