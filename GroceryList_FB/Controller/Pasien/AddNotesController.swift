//  AddNotesController.swift
//  GroceryList_FB
//
//  Created by Jamsey Bond🤪 on 02/05/20.
//  Copyright © 2020 Jamsey Bond🤪. All rights reserved.


import UIKit
import Foundation


protocol AddNotesDelegate {
    
    func AddNotes(controller : UIViewController, notes: PatientNote)// tempat buat nyimpen notesnya
    
}

class AddNotesController: UIViewController {
    
    var delegate : AddNotesDelegate!
    
    
    override func viewDidLoad() {
        
    }
    
    @IBOutlet weak var Notes: UITextView!
    
    @IBAction func addNotes(_ sender: Any) {
        
        if let notes = self.Notes.text {
            
            let patientNote = PatientNote(note: notes)
            self.delegate.AddNotes(controller: self, notes: patientNote)// ini buat nyimpen notesnya di delegate biar bisa di pindah2in
            print(patientNote.note)
            
        }
    }

}
