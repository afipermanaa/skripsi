//
//  NotePatientController.swift
//  GroceryList_FB
//
//  Created by Jamsey Bond🤪 on 08/05/20.
//  Copyright © 2020 Jamsey Bond🤪. All rights reserved.
//

import UIKit
import Firebase

class NotePatientController: UIViewController, AddNotesDelegate, UITextViewDelegate{
    
    func AddNotes(controller: UIViewController, notes: PatientNote) {
        print("🤪")
        
    }
   
    
    var delegate : AddNotesDelegate?

    public var pasienNotes : String = ""
    
    var textDidChangeHandler: (()-> Void)?

    override func viewDidLoad() {
        super.viewDidLoad()

        PatientTextView.text = pasienNotes
        
        updateSaveButtonState()
        
        PatientTextView.delegate = self
        PatientTextView.isEditable = true

    }

    @IBOutlet weak var PatientTextView: UITextView!

    @IBAction func SaveUpdateBtn(_ sender: UIButton) {
        
        if let notes = self.PatientTextView.text {
            let pasienNotes = PatientNote(note: notes)

            self.delegate?.AddNotes(controller: self, notes: pasienNotes)
        }
        
        dismiss(animated: true, completion: nil)
        navigationController?.popViewController(animated: true)
    }
    
    
    func updateSaveButtonState() {

        let text = PatientTextView.text
        self.navigationItem.rightBarButtonItem?.isEnabled = !text!.isEmpty
    }

}
