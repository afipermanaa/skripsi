//
//  PatientNote.swift
//  GroceryList_FB
//
//  Created by Jamsey Bond🤪 on 08/05/20.
//  Copyright © 2020 Jamsey Bond🤪. All rights reserved.
//

import Foundation

struct PatientNote {
    
    var note : String
    
    init(note :String) {
        self.note = note
    }
    
    init?(_ dictionary :[String:Any]){
        
        guard let note = dictionary["note"] as? String else {
            return nil
        }
        
        self.note = note
       
    }
    
    func toDictionary() -> [String:Any] {
        return ["note":self.note]
    }
}
