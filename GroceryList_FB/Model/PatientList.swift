//
//  PatientList.swift
//  GroceryList_FB
//
//  Created by Jamsey Bond🤪 on 08/05/20.
//  Copyright © 2020 Jamsey Bond🤪. All rights reserved.
//

import Foundation

typealias JSONDictionary  = [String:Any]

class PatientList { // patient list ini parent dari patient note buat nyimpen si pasienlist nya🙄
    
    var name : String!
    var patientNote :[PatientNote] = [PatientNote]() //ini buat nyimpen notes2 dari tiap2 pasien 
    
    init(name : String) {
        self.name = name
    }
    
    init?(_ dictionary :[String:Any]){
        //jadi pasien list ngambil informasinya dari dictionary yg di past dr controller, dan dictionary dapet data dari firebasenya, yg dari snapshot itu nanti dr snapshot dimasukin di dictionary ini abis itu di pindahin ke controller
        
        guard let name = dictionary["Name"] as? String else {
            return nil
        }
        
        self.name = name
        let pasienListDictionary = dictionary["patientNote"] as? [JSONDictionary]
        
        if let dictionaries = pasienListDictionary {
            self.patientNote = dictionaries.compactMap(PatientNote.init)
        }
    }
    
    func toDictionary() -> [String:Any] { // ini buat dictionary buat convery object jd string:any, jd biar ga ngubah satu2 kl ada yg salah gituu
        
        return ["Name":self.name, "patientNote":self.patientNote.map{ patientNote in
            return patientNote.toDictionary()
        }]
    }
    
}
