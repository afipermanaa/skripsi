//
//  User + Extension.swift
//  GroceryList_FB
//
//  Created by Jamsey Bond🤪 on 05/06/20.
//  Copyright © 2020 Jamsey Bond🤪. All rights reserved.
//

import Foundation
import Firebase

extension String {
    func removeSpecialChar() -> String {
        return self.components(separatedBy: CharacterSet.letters.inverted).joined()
    }
}

extension User {
    
    var emailWithoutSpecialChar :String {
        
        guard let Email = self.email else {
            fatalError("Unable to Access User Email")
        }
        
        return Email.removeSpecialChar()    }
}
