//
//  SignUpController.swift
//  GroceryList_FB
//
//  Created by Jamsey Bond🤪 on 28/05/20.
//  Copyright © 2020 Jamsey Bond🤪. All rights reserved.
//
import Firebase
import UIKit

class SignUpController: UIViewController {
    
    let identifier : String = "SignUp"

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    @IBOutlet weak var Email: UITextField!
    @IBOutlet weak var Password: UITextField!
    @IBOutlet weak var ReTypePassword: UITextField!
    
    
    @IBAction func SignUpBtn(_ sender: Any) {
        
        if Password.text != ReTypePassword.text{
            let alertController = UIAlertController(title: "Password tidak sama", message: "Please re enter password", preferredStyle: .alert)
            let defaultAction = UIAlertAction(title: "Ok", style: .default, handler: nil)
            alertController.addAction(defaultAction)
            self.present(alertController, animated: true, completion: nil)
        }else{
            Auth.auth().createUser(withEmail: Email.text!, password: Password.text!) { (User, Error) in
                if Error == nil{
                    self.performSegue(withIdentifier: self.identifier, sender: self)
                }else{
                    let alertController = UIAlertController(title: "Error", message: Error?.localizedDescription, preferredStyle: .alert)
                    let defaultAction = UIAlertAction(title: "Ok", style: .cancel, handler: nil)
                    alertController.addAction(defaultAction)
                    self.present(alertController, animated: true, completion: nil)
                }
            }
        }
    }
    
    func textFieldShouldReturn (_ textField : UITextField) -> Bool { //delegate method
        textField.resignFirstResponder()
        return true
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
