//
//  FirstViewController.swift
//  GroceryList_FB
//
//  Created by Jamsey Bond🤪 on 06/06/20.
//  Copyright © 2020 Jamsey Bond🤪. All rights reserved.
//

import UIKit
import Firebase


// ini class buat nentuin user udh login apa blm
// kl udh login user langsung ke main kl blm ke login page.

class FirstViewController: UIViewController {
    
    var authListener : AuthStateDidChangeListenerHandle? //ini buat save ref login nya di func dr firebase

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.checkUserLogin()
       
    
    }
    
    func checkUserLogin() {
        //By using a addStateDidChangelistener, you ensure that the Auth object isn't in an intermediate state—such as initialisation—when you get the current user
              
        authListener = Auth.auth().addStateDidChangeListener({ (auth, user) in
                   if user != nil {
                       //user login
                        self.switchStoryboard()
                       
                   }else{
                       //no login
                       
                   }
               })
    }
    
    func switchStoryboard() {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "Main") as UIViewController
        controller.modalPresentationStyle = .fullScreen

        self.present(controller, animated: false, completion: nil)
    }
    
    

    
//     MARK: - Navigation
//
//     In a storyboard-based application, you will often want to do a little preparation before navigation
    
  

}
