//
//  LoginController.swift
//  GroceryList_FB
//
//  Created by Jamsey Bond🤪 on 28/05/20.
//  Copyright © 2020 Jamsey Bond🤪. All rights reserved.
//


import UIKit
import Firebase

class LoginController: UIViewController {
    
    
    let identifier : String = "Login"

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    @IBOutlet weak var Email: UITextField!
    @IBOutlet weak var Password: UITextField!
    
    
    @IBAction func LoginBtn(_ sender: Any) {
        
        Auth.auth().signIn(withEmail: Email.text!, password: Password.text!) { (user, error) in
            if error == nil {
                self.performSegue(withIdentifier: self.identifier, sender: self)
            }else{
                let alertController = UIAlertController(title: "Error", message: error?.localizedDescription, preferredStyle: .alert)
                let action = UIAlertAction(title: "Ok", style: .cancel, handler: nil)
                alertController.addAction(action)
                self.present(alertController, animated: true, completion: nil)
            }
        }
        
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {   //delegate method
        textField.resignFirstResponder()
        return true
    }
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
